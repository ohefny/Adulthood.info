## Register Or Login 

### ***Url***:
```
api/auth
```

#### ***Request***:
```
POST
```

#### ***Body (application/json)***:
```
{
  "registerOrLogin": {
    "email": "...",
    "password": "...",
    "newUser": "..."
}
```

##### ***Request info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| email | string | unique email | true | |
| password | string | user password | true | |
| newUser | boolean | identifier between a register or login action of a user | false | true/false |


#### ***Response body***:
```
{
  "response": {
    "success": "...",
    "token": "...",
}
```

##### ***Response info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| success | boolean | checks to see if the login or register is a success or not | true | true/false |
| token | string | the jwt token returned from a succesful auth | true | JWT token |

---
## Profile Settings

### ***Url***:
```
api/profile_settings
```

#### ***Request***:
```
PUT
```

#### ***Authorization Header***:
```
JWT-Token
FormData Request
```

#### ***Body (application/json)***:
```
{
  "profileSettings": {
    "email": "...",
    "password": "...",
    "name": "...",
    "is_profile_complete: "..."
    "profile_picture: "..."
}
 ```

##### ***Request info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| email | string | unique email | true | |
| password | string | user password | true | |
| name | string | persons name | true | |
| profile_picture | string | picture of user | false | /folder/name.[jpg,png] |


#### ***Response body***:
```
{
  "response": {
    "success": "...",
    "token": "...",
}
```

##### ***Response info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| success | boolean | checks to see if the put call is a success or not | true | true/false |
| token | string | the jwt token returned from a succesful auth | true | JWT token |


## Get Profile Settings

### ***Url***:
```
api/profile_settings/me
```

#### ***Request***:
```
GET
```

#### ***Authorization Header***:
```
JWT-Token
```

#### ***Body (application/json)***:
```
{
  "userProfile": {
    "email": "...",
    "token": "...",
}
 ```

##### ***Request info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| email | string | unique email | | |
| token | string | JWT | | |


#### ***Request body***:
```
{
  "response": {
    "email": "...",
    "token": "...",
}
 ```

##### ***Request info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| success | boolean | checks to see if the put call is a success or not | true | true/false |
| token | string | the jwt token returned from a succesful auth | true | JWT token |

#### Response Body***(application/json)***:
```
{
  "profileSettings": {
    "email": "...",
    "password": "...",
    "name": "...",
    "is_profile_complete: "..."
    "profile_picture: "..."
}
 ```

##### ***Response info***:
| Name | Type | Description | Required | Pattern |
|:-----|:----:|:------------|:--------:|--------:|
| email | string | unique email | true | |
| password | string | user password | true | |
| name | string | persons name | true | |
| profile_picture | string | picture of user | false | /folder/name.[jpg,png] |

