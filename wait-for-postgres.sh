#!/bin/sh
# wait-for-postgres.sh

set -e

cmd="$@"

echo "starting waiting"

while ! pg_isready -h "postgres" -p "5432" > /dev/null 2> /dev/null; do
   echo "Connecting to postgres Failed"
   sleep 1
done
>&2 echo "--------------------------------------"
>&2 echo "Postgres is up - migrating"
>&2 echo "--------------------------------------"
npm run migrate
>&2 echo "--------------------------------------"
>&2 echo "Migration is done - executing command"
>&2 echo "--------------------------------------"
exec $cmd