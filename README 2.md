#dummy



- uses material-ui for styling
- mocks server response for logging in with name and password and getting user info using jwt token
- login and password are anything, to use actual form values change loginWithCredentials function in app/reduxModules/modules/users.js and change the first app.post function in server.js to test for values you like (now its username 'ryan' and password 'test')
- thought-through routing with a protected page, available only after logging in, redirects to a page that user initially wanted to go but was brought to login because they were not authenticated
- "npm run build","npm start" are used for production, "npm run dev" for development, this setup makes it easier to deploy
- "npm stop" stops the execution even if you logged out of the server and logged in again
- yarn is used for package management (I noticed that it's much faster than npm), "yarn install" to install all dependencies
- webpack 2 is used
- eslintrc included for convinience (install eslint separatly, I prefere having it globally installed)
- an example of using normalizr in included
- PostgreSQL is used (see sample queries in root)
- leverages Docker

to use with docker:

```

docker-compose up

```

to rebuild the containers:

```

docker-compose build

```

to run database migrations:

```

docker exec capstone_web_1 npm run migrate

```

to list all containers

```

docker ps

```

to login to a container

```

docker exec -ti capstone_web_1 sh

```

If you want ot login to psql cli, open new terminal window and run:

```

docker exec -ti capstone_postgres_1 psql ec2-user --username ec2-user

```

to use without docker:

```
  npm i -g yarn
  git clone https://ryan-falcon@bitbucket.org/ryan-falcon/capstone.git
  yarn install
  export PORT="8000"

```

Run with development settings:

```
  npm run dev

```

Set up database on the server:

```

sudo vi /var/lib/pgsql/data/pg_hba.conf

```

On the line with 127.0.0.1 change "ident" to "trust"


DATABASE_URL must be set in etc/environment:

```

DATABASE_URL="postgres://db-user@localhost/pgdb"
DISPLAY=0

```

Then the server must be rebooted:

```

sudo shutdown -r now

```

Run database migrations:

```

npm run migrate

```

Set up port forwarding on the server:

```

sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 5000

```

Run the app with production settings:

```

  npm start &

```

Stop the app in production:

```
  npm stop

```

Run queries on the server:

```
psql
\i select.sql;
\q

```
