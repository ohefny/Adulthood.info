// loads and executes sql files create_tables.sql and insert.sql
'use strict'
let dbm
let type
const fs = require('fs')
const path = require('path')

exports.setup = function (options) {
  dbm = options.dbmigrate
  type = dbm.datatype
}

exports.up = function (db, callback) {
  var filePathCreate = path.join(__dirname + '/create_tables.sql')
  var filePathInsert = path.join(__dirname + '/insert.sql')

  function createT () {
    fs.readFile(filePathCreate, {encoding: 'utf-8'}, function (err, data) {
      if (err) return console.log(err)
      db.runSql(data, function (err) {
        if (err) return console.log(err)
        insertT()
      })
    })
  }

  function insertT () {
    fs.readFile(filePathInsert, {encoding: 'utf-8'}, function (err, data) {
      if (err) return console.log(err)
      db.runSql(data, function (err) {
        if (err) return console.log(err)
        callback()
      })
    })
  }

  createT()
}

exports.down = function (db, callback) {
  var filePath = path.join(__dirname + '/insert.sql')

  fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
    if (err) return console.log(err)
    db.runSql(data, function (err) {
      if (err) return console.log(err)
      callback()
    })
  })
}

exports._meta = {
  'version': 1,
}
