DROP TABLE IF EXISTS answers, questions, users, posts CASCADE;

CREATE TABLE users (
  id SERIAL,
  name varchar(50) NOT NULL,
  username varchar(20) NOT NULL,
  email varchar(20) NOT NULL,
  password varchar(30) NOT NULL,
  tagline varchar(300),
  avatar varchar(300),
  CONSTRAINT pk_users_id PRIMARY KEY (id),
  CONSTRAINT u_users_username UNIQUE(username),
  CONSTRAINT u_users_email UNIQUE(email)
);

CREATE TABLE questions (
  id SERIAL,
  qtext varchar(1000) NOT NULL,
  category varchar(20) NOT NULL,
  title varchar (30) NOT NULL,
  uid integer NOT NULL,
  CONSTRAINT pk_questions_id PRIMARY KEY (id)
);

CREATE TABLE posts (
  id SERIAL,
  ptext varchar(1000) NOT NULL,
  category varchar(20) NOT NULL,
  title varchar (30) NOT NULL,
  uid integer NOT NULL,
  CONSTRAINT pk_posts_id PRIMARY KEY (id)
);

CREATE TABLE answers (
  id SERIAL,
  atext varchar(5000),
  image varchar(300),
  uid integer NOT NULL,
  qid integer NOT NULL,
  sentiment float(8),
  nlu varchar(5000),
  CONSTRAINT pk_answers_id PRIMARY KEY (id),
  CONSTRAINT fk_answers_uid FOREIGN KEY (uid) REFERENCES users(id),
  CONSTRAINT fk_answers_qid FOREIGN KEY (qid) REFERENCES questions(id)
);