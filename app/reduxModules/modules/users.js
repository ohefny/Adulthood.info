// stores user info

import { getLocalToken, setLocalToken, unsetLocalToken } from 'helpers/utils'
import { CALL_API, getJSON } from 'redux-api-middleware'
import { REQUEST_POSTS, RECEIVE_POSTS, FAILURE_POSTS } from './posts'
import _ from 'lodash'
// import jwtDecode from 'jwt-decode'

// constants
const UNAUTH_USER = 'UNAUTH_USER'
const REMOVE_FETCHING_USER = 'REMOVE_FETCHING_USER'
const REMOVE_FETCHING_ACTIVE_USER = 'REMOVE_FETCHING_ACTIVE_USER'
const REQUEST_USER = '/user/REQUEST'
const REQUEST_ACTIVE_USER = '/user/REQUEST_ACTIVE'
const FAILURE_ACTIVE_USER = '/user/FAILURE_ACTIVE'

const RECEIVE_USER = '/user/RECEIVE'
const RECEIVE_ACTIVE_USER = '/user/RECEIVE_ACTIVE'
const FAILURE_USER = '/user/FAILURE'

// Action creators

export function logoutAndUnauth () {
  unsetLocalToken()
  return {
    type: UNAUTH_USER,
  }
}

export function removeFetchingUser () {
  return {
    type: REMOVE_FETCHING_USER,
  }
}

export function removeFetchingActiveUser () {
  return {
    type: REMOVE_FETCHING_ACTIVE_USER,
  }
}

export function getUser (username) {
  return {
    [CALL_API]: {
      endpoint: `/db/username/${username}`,
      method: 'GET',
      headers: {'Authorization': `Bearer ${getLocalToken()}`},
      types: [
        REQUEST_USER,
        {
          type: RECEIVE_USER,
          payload: (action, state, res) => {
            const result = getJSON(res).then((data) => {
              return data
            })
            return result
          },
        },
        FAILURE_USER,
      ],
    },
  }
}

// export function addUser (name, email, password, avatar = '') {
//   return {
//     [CALL_API]: {
//       endpoint: `/db/users/`,
//       method: 'POST',
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//       },
//       types: [REQUEST_USER, RECEIVE_USER, FAILURE_USER],
//       body: JSON.stringify({
//         name: name,
//         email: email,
//         password: password,
//         avatar: avatar,
//       }),
//     },
//   }
// }

export function loginWithCredentials (username, password) {
  return {
    [CALL_API]: {
      endpoint: `/auth/login`,
      method: 'post',
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({username: username, password: password}),
      types: [
        REQUEST_ACTIVE_USER,
        {
          type: RECEIVE_ACTIVE_USER,
          payload: (action, state, res) => {
            const result = getJSON(res).then((data) => {
              setLocalToken(data.token)
              return data
            })
            return result
          },
        },
        FAILURE_ACTIVE_USER,
      ],
    },
  }
}

function fetchActiveUser () {
  return {
    [CALL_API]: {
      endpoint: `/auth/user_info`,
      method: 'GET',
      headers: {'Authorization': `Bearer ${getLocalToken()}`},
      types: [REQUEST_ACTIVE_USER, RECEIVE_ACTIVE_USER, FAILURE_ACTIVE_USER],
    },
  }
}

export function getActiveUser () {
  return function (dispatch) {
    return Promise.resolve(dispatch(fetchActiveUser()))
  }
}
// initial state

const initialState = {
  isFetching: true,
  isFetchingActive: true,
  error: '',
  isAuthed: false,
  authedUsername: '',
  usernames: [],
}

// Reducers

export default function users (state = initialState, action) {
  switch (action.type) {
    case UNAUTH_USER :
      return {
        ...state,
        isAuthed: false,
        authedUsername: '',
      }
    case REQUEST_USER:
      return {
        ...state,
        isFetching: true,
      }
    case REQUEST_ACTIVE_USER:
      return {
        ...state,
        isFetchingActive: true,
      }
    case REMOVE_FETCHING_ACTIVE_USER :
      return {
        ...state,
        isFetchingActive: false,
      }
    case REMOVE_FETCHING_USER :
      return {
        ...state,
        isFetching: false,
      }
    case RECEIVE_ACTIVE_USER:
      return !action.payload.user
        ? {
          ...state,
          isFetchingActive: false,
          error: '',
        }
        : {
          ...state,
          isFetchingActive: false,
          error: '',
          [action.payload.user.username]: action.payload.user,
          isAuthed: true,
          authedUsername: action.payload.user.username,
          usernames: _.union([action.payload.user.username], state.usernames),
        }
    case RECEIVE_USER:
      return !action.payload.username
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          [action.payload.username]: action.payload,
          usernames: _.union([action.payload.username], state.usernames),
        }
    case RECEIVE_POSTS:
      console.log(Object.keys(action.payload.entities.users))
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...action.payload.entities.users,
          usernames: _.union(Object.keys(action.payload.entities.users), state.usernames),
        }
    case FAILURE_USER:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
      }
    case FAILURE_ACTIVE_USER:
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
      }
    default :
      return state
  }
}
