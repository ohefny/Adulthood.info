import React from 'react'
import PropTypes from 'prop-types'
import { UserPosts } from 'components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as userActionCreators from 'reduxModules/modules/users'
import * as postsActionCreators from 'reduxModules/modules/posts'
import * as userPostsActionCreators from 'reduxModules/modules/userPosts'

const UserPostsContainer = React.createClass({
  propTypes: {
    // users
    error: PropTypes.string.isRequired,
    isFetching: PropTypes.bool.isRequired,
    users: PropTypes.object.isRequired,
    
    getUser: PropTypes.func.isRequired,
    getPost: PropTypes.func.isRequired,
    addUserPost: PropTypes.func.isRequired,
  },

  contextTypes: {
    router: PropTypes.object.isRequired,
  },
  // fecth smth on load here
  componentWillMount () {

  },

  styles: {
    buttonStyle: {
      marginBottom: 20,
    },
  },

  // fetchUser () {
  //   this.props.getUser('123')
  // },

  // fetchPost () {
  //   this.props.getPost('123')
  // },

  addPost () {
    // this.props.addUser('alex', 'al@l.c', 'asd', 'sda')
    this.props.addUserPost()
  },

  render () {
    return <UserPosts
      isFetching={this.props.isFetching}
      error={this.props.error}
      styles={this.styles}
      user={this.props.users[this.props.params.username]}
      addPost={this.addPost}/>
  },
})

function mapStateToProps ({users}, props) {
  const { error, isFetching } = users
  return {
    error,
    isFetching,
    users,
  }
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators(
    {
      ...userActionCreators,
      ...postsActionCreators,
      ...userPostsActionCreators,
    },
     dispatch
  )
)(UserPostsContainer)
