import React from 'react'
import PropTypes from 'prop-types'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { PostSet } from 'components'

import CircularProgress from 'material-ui/CircularProgress'

import * as userPostsActionCreators from 'reduxModules/modules/userPosts'

const PostSetContainer = React.createClass({
  propTypes: {
    getUserPosts: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    posts: PropTypes.object.isRequired,
    postIds: PropTypes.array.isRequired,
    userId: PropTypes.number.isRequired,
  },

  componentDidMount () {
    this.props.getUserPosts(this.props.userId)
  },

  render () {
    const nodes = this.props.postIds.map(nodeId => this.props.posts[nodeId])
    return this.props.isFetching ? <CircularProgress /> : (
      <PostSet nodes={nodes}/>
    )
  },
})

function mapStateToProps ({userPosts, posts}, props) {
  const { error, isFetching, postIds } = userPosts
  return {
    error,
    isFetching,
    posts,
    postIds,
  }
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators(
    {
      ...userPostsActionCreators,
    },
     dispatch
  )
)(PostSetContainer)
