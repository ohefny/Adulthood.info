import React, { PropTypes } from 'react'
import { Navigation, Footer } from 'components'
import { mainContainer, innerContainer } from './styles.css'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActionCreators from 'reduxModules/modules/users'
import { checkToken } from 'helpers/utils'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

const MainContainer = React.createClass({
  propTypes: {

    // users
    isAuthed: PropTypes.bool.isRequired,
    isFetchingActive: PropTypes.bool.isRequired,
    getActiveUser: PropTypes.func.isRequired,
    removeFetchingActiveUser: PropTypes.func.isRequired,
    getUser: PropTypes.func.isRequired,
  },
  contextTypes: {
    router: PropTypes.object.isRequired,
  },
  componentWillMount () {
    // check if authed and if not, login with token if possible
    if (this.props.isAuthed) {
      this.props.removeFetchingUser()
    //  var username = this.props.params.username
     // this.props.getUser(username)
    } else {
      // if not authed but there is a token
        // not authed, no token present
      //this.props.removeFetchingActiveUser()
      this.context.router.push('/auth')
    }
  },

  componentDidMount () {
    // from the path `/inbox/messages/:id`

  },

  // exit active nomination and go to home screen
  goHome () {
    this.context.router.push('/')
  },

  // logout completely
  handleLogout () {
    this.context.router.push('/logout')
  },

  render () {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div className={mainContainer}>
          <Navigation goHome={this.goHome} logout={this.handleLogout}/>
          <div className={innerContainer}>
              {this.props.isFetchingActive === true
                ? null
                : this.props.children}
          </div>
          <Footer/>
        </div>
      </MuiThemeProvider>
    )
  },
})

export default connect(
  ({users}) => ({
    isAuthed: users.isAuthed,
    isFetching: users.isFetching,
    isFetchingActive: users.isFetchingActive,
  }),
  (dispatch) => bindActionCreators({
    ...userActionCreators,
  }, dispatch)
   )(MainContainer)
