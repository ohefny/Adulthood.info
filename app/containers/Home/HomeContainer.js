import React from 'react'
import PropTypes from 'prop-types'
import { Home } from 'components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as userActionCreators from 'reduxModules/modules/users'
import * as postsActionCreators from 'reduxModules/modules/posts'
import * as userPostsActionCreators from 'reduxModules/modules/userPosts'

const HomeContainer = React.createClass({
  propTypes: {
    // users
    error: PropTypes.string.isRequired,
    isFetching: PropTypes.bool.isRequired,
    users: PropTypes.object.isRequired,
    posts: PropTypes.object.isRequired,

    getUser: PropTypes.func.isRequired,
    getPost: PropTypes.func.isRequired,
    getPosts: PropTypes.func.isRequired,
    addUserPost: PropTypes.func.isRequired,
  },

  contextTypes: {
    router: PropTypes.object.isRequired,
  },
  // fecth smth on load here
  componentWillMount () {
    this.props.getPosts()
  },

  styles: {
    buttonStyle: {
      marginBottom: 20,
    },
  },

  // fetchUser () {
  //   this.props.getUser('123')
  // },

  // fetchPost () {
  //   this.props.getPost('123')
  // },

  addPost () {
    // this.props.addUser('alex', 'al@l.c', 'asd', 'sda')
    this.props.addUserPost()
  },

  render () {
    return <Home
      isFetching={this.props.isFetching}
      error={this.props.error}
      styles={this.styles}
      posts={this.props.posts}
      addPost={this.addPost}/>
  },
})

function mapStateToProps ({users, posts}, props) {
  const { error, isFetching } = posts
  return {
    error,
    isFetching,
    posts,
    users,
  }
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators(
    {
      ...userActionCreators,
      ...postsActionCreators,
      ...userPostsActionCreators,
    },
     dispatch
  )
)(HomeContainer)
