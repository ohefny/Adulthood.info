import React from 'react'
import PropTypes from 'prop-types'
import { Post } from 'components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as userPostsActionCreators from 'reduxModules/modules/userPosts'

import { analyze } from 'helpers/api'
import { parseJSON } from 'helpers/utils'

// import { d3 } from 'd3'
import cloud from 'd3-cloud'
const d3 = require('d3')
// import d3 from 'd3'

const PostContainer = React.createClass({
  propTypes: {
    post: PropTypes.object.isRequired,
    size: PropTypes.string.isRequired,
    style: PropTypes.string,
    addUserPost: PropTypes.func.isRequired,
  },
  getInitialState () {
    return {
      open: false,
    }
  },
  componentDidMount () {
    //this.draw(this.props.post.id, this.props.post.nlu)
  },
  handleClick () {
    //this.setState({open: !this.state.open})

    // I used this code to test api calls to ibm watson

    // analyze('I helped my team members on the dance pal with a move she had really been struggling with. I knew that she had missed the last few practices but I realized that the whole team’s performance depended on her being able to do the move. So I stayed extra late and helped her practice')
    // .then(data => parseJSON(data))
    // .then(res=> console.log(JSON.stringify(res)))
    // this.props.addUserPost({
    //   atext: 'One time in my creative writing class we were assigned to write a one-act play. I thought it was going to be awesome, until my professor decided to force us to work in partners. My friend Alex was absent that day and so I asked my professor if Alex could be my partner. My professor agreed and so I sent a message to Alex saying that we were partners for this project! I didn’t get a response until the next day when Alex said she was dropping the class. My heart stopped and I franticly went to my professor’s office and told her the situation and she said that there was nothing she could do. So I begged Alex to reconsider. After a few hours Alex got back to me saying, oh sorry Jamie lol, I meant to send that to Mark. So Alex was never going to drop the class! Instead, Alex wanted to drop calculus. ',
    //   image: '/pictures/1.jpg',
    //   uid: 2,
    //   qid: 2,
    //   sentiment: 0.2,
    // })
  },

  // draw (id, nlu) {
  //   let words
  //   if (nlu) {
  //     // get data
  //     const keywords = [...nlu.keywords, ...nlu.concepts]
  //     keywords.map(word => {
  //       word.relevance = word.relevance * 100
  //     })
  //     //console.log(keywords)
  //     words = keywords
  //   } else {
  //     // stub for the nlu data
  //     words = [
  //       {'text': 'study', 'relevance': 40},
  //       {'text': 'motion', 'relevance': 15},
  //       {'text': 'forces', 'relevance': 10},
  //       {'text': 'electricity', 'relevance': 15},
  //       {'text': 'movement', 'relevance': 10},
  //       {'text': 'relation', 'relevance': 10},
  //       {'text': 'things', 'relevance': 10},
  //       {'text': 'force', 'relevance': 5},
  //       {'text': 'ad', 'relevance': 5},
  //       {'text': 'energy', 'relevance': 85},
  //       {'text': 'living', 'relevance': 10},
  //       {'text': 'nonliving', 'relevance': 10},
  //       {'text': 'laws', 'relevance': 15},
  //       {'text': 'speed', 'relevance': 45},
  //       {'text': 'velocity', 'relevance': 30},
  //       {'text': 'define', 'relevance': 10},
  //       {'text': 'constraints', 'relevance': 10},
  //       {'text': 'universe', 'relevance': 10},
  //       {'text': 'physics', 'relevance': 100},
  //       {'text': 'describing', 'relevance': 10},
  //       {'text': 'matter', 'relevance': 90},
  //       {'text': 'physics-the', 'relevance': 10},
  //       {'text': 'world', 'relevance': 10},
  //       {'text': 'works', 'relevance': 10},
  //       {'text': 'science', 'relevance': 70},
  //       {'text': 'interactions', 'relevance': 30},
  //       {'text': 'studies', 'relevance': 5},
  //       {'text': 'properties', 'relevance': 45},
  //       {'text': 'nature', 'relevance': 40},
  //       {'text': 'defintions', 'relevance': 10},
  //       {'text': 'two', 'relevance': 15},
  //       {'text': 'grouped', 'relevance': 15},
  //       {'text': 'traditional', 'relevance': 15},
  //       {'text': 'fields', 'relevance': 15},
  //       {'text': 'acoustics', 'relevance': 15},
  //       {'text': 'optics', 'relevance': 15},
  //       {'text': 'mechanics', 'relevance': 20},
  //       {'text': 'thermodynamics', 'relevance': 15},
  //       {'text': 'electromagnetism', 'relevance': 15},
  //       {'text': 'modern', 'relevance': 15},
  //       {'text': 'extensions', 'relevance': 15},
  //       {'text': 'thefreedictionary', 'relevance': 15},
  //       {'text': 'interaction', 'relevance': 15},
  //       {'text': 'org', 'relevance': 25},
  //       {'text': 'department', 'relevance': 10},
  //       {'text': 'gravitation', 'relevance': 10},
  //       {'text': 'heat', 'relevance': 10},
  //       {'text': 'light', 'relevance': 10},
  //       {'text': 'magnetism', 'relevance': 10},
  //       {'text': 'modify', 'relevance': 10},
  //       {'text': 'general', 'relevance': 10},
  //       {'text': 'bodies', 'relevance': 15},
  //       {'text': 'philosophy', 'relevance': 15},
  //       {'text': 'brainyquote', 'relevance': 15},
  //       {'text': 'words', 'relevance': 15},
  //       {'text': 'ph', 'relevance': 15},
  //       {'text': 'html', 'relevance': 15},
  //       {'text': 'lrl', 'relevance': 15},
  //       {'text': 'zgzmeylfwuy', 'relevance': 15},
  //       {'text': 'subject', 'relevance': 15},
  //       {'text': 'distinguished', 'relevance': 15},
  //     ]
  //   }

  //   // draw the cloud

  //   var width = 500
  //   var height = 500

  //    //var color = d3.scale.category20()
  //    //reds
  //   var color1 = d3.scaleLinear().domain([0, 1, 2, 3, 4, 5, 6, 10, 15, 20, 100])
  //  .range(['#F00', '#E00', '#D00', '#C00', '#B00', '#A00', '#900', '#800', '#700', '#600', '#500', '#400'])

  //  //multiple colours
  //   var color = d3.scaleLinear().domain([0, 1, 2, 3, 4, 5, 6, 10, 15, 20, 100])
  //  .range(['#78aae6', '#6D0E0D', '#5BAF76', '#682220', '#BC1916', '#49B6CE', '#FFBA08', '#7C6938', '#78aae6', '#EAA04B', '#78aae6', '#BC6E6D'])


  //   // var vis = document.getElementById('wordcloud')
  //   var layout = cloud().size([width, height])
  //     .words(words)
  //     .padding(2)
  //     .rotate(function () { return (~~(Math.random() * 1) - 1) * 20 })
  //     .font('Raleway')
  //     .fontWeight(900)
  //     .fontSize(function (d) { return d.relevance })
  //     .spiral('archimedean')
  //     .on('end', draw)

  //   layout.start()

  //   function draw (words) {
  //     d3.select('#wordcloud' + id).append('svg')
  //       .style('width', '100%')
  //       .style('height', '100%')
  //       .attr('viewBox', '0 0 ' + width + ' ' + height)
  //       .attr('preserveAspectRatio', 'xMidYMin meet')
  //       .append('g')
  //       .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
  //       .selectAll('text')
  //       .data(words).enter().append('text')
  //       .style('font-size', function (d) { return d.size + 'px' })
  //       .style('font-family', 'Raleway')
  //       .style('font-weight', 900)
  //       .style('fill', function (d, i) { return color(i) })
  //       .attr('text-anchor', 'middle')
  //       .attr('transform', function (d) {
  //         return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')'
  //       })
  //       .text(function (d) { return d.text })
  //   }
  // },
  render () {
    return (
      <Post
        post={this.props.post}
        size={this.props.size}
        style={this.props.style}
        open={this.state.open}
        handleClick={this.handleClick}
        />
    )
  },
})

function mapStateToProps () {
  return {}
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators(
    {
      ...userPostsActionCreators,
    },
     dispatch
  )
)(PostContainer)
