// const protocol = 'http://'
// const localServer = 'localhost'
// const port = ':9080'
// const remoteServer = 'remote.com'
// const version = '/api-v1'

// const localhost = ''
// const remote = protocol + remoteServer + version

// const rootFolder = localhost
// const rootFolder = remote
import fetch from 'isomorphic-fetch'

// ================================Authentication========================================
// export function login (username, password) {
//   return fetch('/auth/login', {
//     method: 'post',
//     credentials: 'include',
//     headers: {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({username: '123', password: 'test'}),
//   })
// }

// export function fetchUser (token) {
//   return fetch('/auth/user_info', {
//     credentials: 'include',
//     headers: {
//       'Authorization': `Bearer ${token}`,
//     },
//   })
// }
// ============================================================================
// using natural language understanding (ibm watson)
export function analyze (text) {
  const url = 'https://pyfe.mybluemix.net/api/profile'
  const options = {
    html: text,
    features: {
      concepts: {},
      keywords: {},
    },
  }

  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify(options),
  })
}
