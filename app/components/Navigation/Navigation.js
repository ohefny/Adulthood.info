// this component is currently not in use
import React, { PropTypes } from 'react'
import { container, navContainer, btn } from './styles.scss'
import FlatButton from 'material-ui/FlatButton'

Navigation.propTypes = {
  goHome: PropTypes.func.isRequired,

  // parent: MainContainer.js
  logout: PropTypes.func.isRequired,
}

export default function Navigation ({goHome, logout}) {
  return (
    <div className={container}>
      <nav className={navContainer}>
        <FlatButton
          className={btn}
          label='Back to Home'
          primary={true}
          onTouchTap={goHome}/>
        <FlatButton
          className={btn}
          label='Logout'
          primary={true}
          onTouchTap={logout}/>
      </nav>
    </div>
  )
}
