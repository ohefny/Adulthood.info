import React from 'react'
import PropTypes from 'prop-types'
import { footerContainer, entry } from './styles.scss'

Footer.propTypes = {
  //: PropTypes.string.isRequired,
}

export default function Footer (props) {
  return (
    <div className={footerContainer}>
      <h4 className={entry}> React Redux Docker PostgreSQL </h4>
      <h4 className={entry}>© 2017 Ryan Falcon </h4>
    </div>
  )
}
