import React from 'react'
import PropTypes from 'prop-types'
import { Card } from 'components'

Post.propTypes = {
  post: PropTypes.object.isRequired,
  style: PropTypes.string,
}

export default function Post ({post, style}) {

  return (
    <div className={style}>
      <Card
        text={post.ptext}
        title={post.title}
        date={post.date || '1 Jan 2017'}/>
    </div>
  )
}
