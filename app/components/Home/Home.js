import React from 'react'
import PropTypes from 'prop-types'
import {container, name, slogan, nameLine, avatar} from './styles.scss'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'

import { PostSetContainer } from 'containers'

Home.propTypes = {
  // container
  styles: PropTypes.object.isRequired,
  // users
  error: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
  addPost: PropTypes.func.isRequired,
  posts: PropTypes.object,

}

export default function Home ({isFetching, error, styles, posts, addPost}) {
  let { buttonStyle } = styles
  const size = 300
  const avatarStyle = {
    width: size,
    height: size,
  }

  return isFetching ? <CircularProgress /> : (error !== '' ? <div> Error </div> : (
      <div className={container}>
        
        All posts will be here
        <p hidden> {error} </p>
          {/*<RaisedButton onTouchTap={addPost} label='Add Post'/>*/}
          {/* <TimelineContainer userId={user.id}/> */}
        

      </div>
  ))
}
