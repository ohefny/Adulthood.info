import React from 'react'
import PropTypes from 'prop-types'
import { PostContainer } from 'containers'

PostSet.propTypes = {
  nodes: PropTypes.array.isRequired,
}

export default function PostSet ({nodes}) {
  return (
    <div >
      {nodes.map(node => <PostContainer post={node}/>)}
    </div>
  )
}
