// this component is currently not in use
import React, { PropTypes } from 'react'
import { default as MaterialAvatar } from 'material-ui/Avatar'

Avatar.propTypes = {
  base64: PropTypes.string,
  url: PropTypes.string,
  style: PropTypes.object,
}

export default function Avatar ({url, base64, style = {}}) {
  // if there is an avatar, display it, or show 1 transparent pixel
  const size = 200
  const styles = {
    width: size,
    height: size,
    objectFit: 'cover',
    ...style,
  }

  if (url) {
    return (
      <MaterialAvatar src={url} alt='profile picture' style={styles}/>
    )
  }

  const imageString = base64 || 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
  const srcString = 'data:image/jpeg;base64, ' + imageString

  return (
    <MaterialAvatar src={srcString} alt='profile picture' style={styles}/>
  )
}
