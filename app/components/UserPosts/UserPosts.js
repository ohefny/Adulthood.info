import React from 'react'
import PropTypes from 'prop-types'
import {container, name, slogan, nameLine, avatar} from './styles.scss'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'

import { PostSetContainer } from 'containers'

UserPosts.propTypes = {
  // container
  styles: PropTypes.object.isRequired,
  // users
  error: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
  addPost: PropTypes.func.isRequired,
  user: PropTypes.object,

}

export default function UserPosts ({isFetching, error, styles, user, addPost}) {
  let { buttonStyle } = styles
  const size = 300
  const avatarStyle = {
    width: size,
    height: size,
  }

  return isFetching ? <CircularProgress /> : (error !== '' ? <div> User not found </div> : (
      <div className={container}>
        
        <div className={nameLine}>
          <img src={user.avatar} className={avatar}/>
        </div>
        <div className={name}> {user.name} </div>
        <p className={slogan}> {user.tagline} </p>
        <p hidden> {error} </p>
          {/*<RaisedButton onTouchTap={addPost} label='Add Post'/>*/}
          {/* <TimelineContainer userId={user.id}/> */}
          <PostSetContainer userId={user.id}/>

      </div>
  ))
}
