import React from 'react'
import PropTypes from 'prop-types'
import {card, titleStyle} from './styles.scss'

Card.propTypes = {
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  location: PropTypes.string,
}

export default function Card ({text, title, date, location}) {
  return (
    <div className={card}>
      <h2 className={titleStyle}>{title}</h2>
      <h3>{text}</h3>
      <h4>{date}</h4>
      <h4>{location}</h4>
    </div>
  )
}
