// Production settings for the server
// -------------------------------------
// this stuff is for Bluemix

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
// var cfenv = require('cfenv')

// get the app environment from Cloud Foundry
// var appEnv = cfenv.getAppEnv()

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express')

const common = require('./server')
var compression = require('compression')
var path = require('path')
const port =  process.env.PORT || 5000

// create a new express server
const app = express()

// Production
app.use(compression())

// // landing page
// app.get('/', function response (req, res) {
//   res.sendFile(path.join(__dirname, '/../build_client/landing.html'))
// })

// static stuff
app.use(express.static(__dirname + '/../build_client'))

common.setApp(app)

app.get('/*', function response (req, res) {
  res.sendFile(path.join(__dirname, '/../build_client/index.html'))
})

common.listen(app, port)
