// Server settings that are common for both production and development
// import ErrorHandler from 'express-simple-errors'

/* eslint-env node */

/* eslint no-console: 0 */
import routes from './db/routes'
require('babel-polyfill')

const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const secret = 'secret-key'
const fs = require('fs')
const request = require('request')

process.title = process.argv[2]
const port = process.env.PORT || 5000

module.exports = {
  setApp: function setApp (app) {
    app.use(bodyParser.json())

    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
      extended: true,
    }))

    // TODO: replace with actual authentication
    app.post('/auth/login', (req, res) => {
      if (req.body.username === 'ryan' && req.body.password === 'test') {
        // var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
        const uid = 1
        request('http://localhost:' + port + '/db/users/' + uid, (err, r, body) => {
          if (err) {
            console.log('error', err)
            res.sendStatus(401)
          }
          // console.log('body:', body)
          const user = JSON.parse(body)
          var token = jwt.sign({ uid: user.id }, secret)
          var result = {
            user: user,
            token: token,
          }
          res.status(200).json(result)
        })
      } else {
        res.sendStatus(403)
      }
    })

    // get info of a logged in user
    app.get('/auth/user_info', (req, res) => {
      var token = req.headers['authorization']
      if (!token) {
        res.sendStatus(401)
      } else {
        try {
          var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
          if (decoded.uid !== 1) {
            res.sendStatus(403)
          } else {
            request('http://localhost:' + port + '/db/users/' + decoded.uid, (err, r, body) => {
              if (err) {
                console.log('error', err)
                res.sendStatus(401)
              }
              // console.log('body:', body)
              const user = JSON.parse(body)
              res.status(200).json({
                user: user,
                token: token,
              })
            })
          }
        } catch (e) {
          res.sendStatus(403)
        }
      }
    })

    // // get info for a specific user by user id
    // app.get('/users/:id', (req, res) => {
    //   var token = req.headers['authorization']
    //   if (!token) {
    //     res.sendStatus(401)
    //   } else {
    //     try {
    //       var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
    //       res.status(200).json({
    //         id: req.params.id,
    //         username: req.params.id,
    //         display_name: 'John Smith',
    //       })
    //     } catch (e) {
    //       res.sendStatus(401)
    //     }
    //   }
    // })

    // // get a post by id
    // app.get('/posts/:id', (req, res) => {
    //   var token = req.headers['authorization']
    //   if (!token) {
    //     res.sendStatus(401)
    //   } else {
    //     try {
    //       var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
    //       res.status(200).json({
    //         id: req.params.id,
    //         author: {
    //           id: '1',
    //           username: 'johns',
    //           display_name: 'John Smith',
    //         },
    //         title: 'My awesome blog post',
    //         comments: [
    //           {
    //             id: '324',
    //             commenter: {
    //               id: '2',
    //               username: 'nickdoe',
    //               display_name: 'Nick Doe',
    //             },
    //           },
    //         ],
    //       })
    //     } catch (e) {
    //       res.sendStatus(401)
    //     }
    //   }
    // })

    // get posts of a user with a specific id
    app.get('/userPosts/:id', (req, res) => {
      var token = req.headers['authorization']
      if (!token) {
        res.sendStatus(401)
      } else {
        try {
          var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
          request('http://localhost:' + port + '/db/user_posts/' + req.params.id, (err, r, body) => {
            if (err) {
              console.log('error', err)
              res.sendStatus(401)
            }
            // console.log('body:', body)
            const result = { id: req.params.id, nodes: JSON.parse(body).reverse() }
            res.status(200).json(result)
          })
        } catch (e) {
          res.sendStatus(401)
        }
      }
    })

     // get posts of a user with a specific id
     app.get('/posts', (req, res) => {
      var token = req.headers['authorization']
      if (!token) {
        res.sendStatus(401)
      } else {
        try {
          var decoded = jwt.verify(token.replace('Bearer ', ''), secret)
          request('http://localhost:' + port + '/db/posts/' + req.params.id, (err, r, body) => {
            if (err) {
              console.log('error', err)
              res.sendStatus(401)
            }
            // console.log('body:', body)
            
            const result = {  posts: JSON.parse(body) }
            res.status(200).json(result)
          })
        } catch (e) {
          res.sendStatus(401)
        }
      }
    })

    // other routes (db-related)
    app.use('/db', routes())

  },

  listen: function listen (app, port) {
    // this will be printed to the console when the server starts
    app.listen(port, function onStart (err) {
      if (err) {
        console.log(err)
      }
      console.info('==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
    })
  },
}
