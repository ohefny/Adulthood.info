export const userSchema = {
  name: {
    type: String,
    required: true,
    errors: {
      type: 'Name must be a string.',
      required: 'Name is required.',
    },
  },
  email: {
    type: String,
    required: true,
    errors: {
      type: 'email must be a string.',
      required: 'email is required.',
    },
  },
  username: {
    type: String,
    required: true,
    errors: {
      type: 'username must be a string.',
      required: 'username is required.',
    },
  },
  tagline: {
    type: String,
    required: false,
    errors: {
      type: 'tagline must be a string.',
    },
  },
  password: {
    type: String,
    required: true,
    errors: {
      type: 'password must be a string.',
      required: 'password is required.',
    },
  },
  avatar: {
    type: String,
    required: false,
    errors: {
      type: 'avatar must be a string.',
    },
  },
}

export const questionSchema = {
  qtext: {
    type: String,
    required: true,
    errors: {
      type: 'qtext must be a string.',
      required: 'qtext is required.',
    },
  },
  category: {
    type: String,
    required: true,
    errors: {
      type: 'type must be a string.',
      required: 'image is required.',
    },
  },
  title: {
    type: String,
    required: true,
    errors: {
      type: 'title must be a string.',
    },
  },
  uid: {
    type: Number,
    required: true,
    errors: {
      type: 'uid must be a Number.',
      required: 'uid is required.',
    },
  },
}

export const postSchema = {
  ptext: {
    type: String,
    required: true,
    errors: {
      type: 'qtext must be a string.',
      required: 'qtext is required.',
    },
  },
  category: {
    type: String,
    required: true,
    errors: {
      type: 'type must be a string.',
      required: 'image is required.',
    },
  },
  title: {
    type: String,
    required: true,
    errors: {
      type: 'title must be a string.',
    },
  },
  uid: {
    type: Number,
    required: true,
    errors: {
      type: 'uid must be a Number.',
      required: 'uid is required.',
    },
  },
}

export const answerSchema = {
  atext: {
    type: String,
    required: true,
    errors: {
      type: 'atext must be a string.',
      required: 'atext is required.',
    },
  },
  uid: {
    type: Number,
    required: true,
    errors: {
      type: 'uid must be a Number.',
      required: 'uid is required.',
    },
  },
  qid: {
    type: Number,
    required: true,
    errors: {
      type: 'qid must be a Number.',
      required: 'qid is required.',
    },
  },
  image: {
    type: String,
    required: false,
    errors: {
      type: 'image must be a string.',
    },
  },
  sentiment: {
    type: Number,
    required: false,
    errors: {
      type: 'sentiment must be a Number.',
    },
  },
  nlu: {
    type: String,
    required: false,
    errors: {
      type: 'nlu must be a string.',
    },
  },
}

function cleanUpObject (obj, table, baseUrl) {
  for (var [key, value] of Object.entries(obj)) {
    if (value === null || value === undefined) delete obj[key]
  }

  obj.url = `${baseUrl}/${table}/${obj.id}`
  return obj
}

export default function transformResponse (result, type, table) {
  if (result[table]) {
    result = result[table].map((obj) => cleanUpObject(obj, table, result.baseUrl))
  }

  if (result[type]) {
    result = cleanUpObject(result[type], type, result.baseUrl)
  }

  return result
}
