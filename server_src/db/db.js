// postgresql database operations
import pg from 'pg'
import { parse } from 'pg-connection-string'

var conString = process.env.DATABASE_URL
var config = parse(conString)

var pool = new pg.Pool(config)
pool.on('error', function (err, client) {
  console.error('idle client error', err.message, err.stack)
})

function query (query) {
  return pool.query(query)
  .then((result) => {
    return result.rows
  })
  .catch((err) => {
    return console.error('error running query', err)
  })
}

function normalizeValue (value) {
  if (typeof value === 'string') {
    return `'${value}'`
  }
  return value
}

function all (table) {
  return query(`SELECT * FROM ${table}`)
}

function clear (table) {
  return query(`DELETE FROM ${table}`)
}

function create (table, params) {
  const assigns = Object.keys(params)
  const values = Object.values(params).map((value) => normalizeValue(value))
  return query(`INSERT INTO ${table} (${assigns}) VALUES (${values}) RETURNING *`)
}

function getById (table, id) {
  return query(`SELECT * FROM ${table} WHERE id=${id}`)
}

function getPostsByUserId (id) {
  return query(`SELECT p.id, p.ptext, p.uid, p.title FROM posts p WHERE p.uid=${id} `)
}

function getUserByUsername (username) {
  return query(`SELECT * FROM users WHERE username='${username}'`)
}

function update (table, id, params) {
  if (params.id) delete params.id
  const assigns = Object.keys(params)
  const values = assigns.map((key) => `${key}=${normalizeValue(params[key])}`).join(', ') // eslint-disable-line
  return query(`UPDATE ${table} SET ${values} WHERE id=${id} RETURNING *`)
}

function deleteById (table, id) {
  return query(`DELETE FROM ${table} WHERE id = ${id}`)
}

export default {all, clear, create, deleteById, getById, getPostsByUserId, getUserByUsername, update}
