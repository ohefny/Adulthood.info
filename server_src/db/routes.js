// db manipulation routes for the app
import express from 'express'
import { validate } from 'isvalid'
import { errors } from 'express-simple-errors'
import transformResponse, { userSchema, questionSchema, answerSchema, postSchema } from './model' // eslint-disable-line no-unused-variables
import db from './db'
import { analyze } from '../../app/helpers/api'

const userTable = 'users'
const userType = 'user'
const questionTable = 'questions'
const questionType = 'question'
const answerTable = 'answers'
const answerType = 'answer'
const postTable = 'posts'
const postType = 'post'

const tables = [userTable, questionTable, postTable]
const types = [userType, questionType, postType]
const schemas = [userSchema, questionSchema, postSchema]

export default function () {
  const router = express.Router()

  // creates same kind of routes for users, questions, posts
  types.forEach((type, index) => {
    const table = tables[index]
    const schema = schemas[index]
    router.route('/' + table)
    .get(getAll(table), returnResponse(type, table))
    .post(val(schema), insertItem(type, table), returnResponse(type, table))
    .delete(clearTable, returnResponse(type, table))

    router.route('/' + table + '/:id')
      .all(getOne(type, table))
      .get(returnResponse(type, table))
      .patch(updateOne(type, table), returnResponse(type, table))
      .delete(deleteOne(type, table), returnResponse(type, table))

    // special route for user answers, posts, questions
    
    router.route('/user_' + table + '/:id')
      .get(getUsersItems(table), returnResponse(type, table))
  
  })

  // get user by username instead of id
  router.route('/username/:id')
      .all(getByUsername)
      .get(returnResponse(userType, userTable))
      .patch(updateOne(userType, userTable), returnResponse(userType, userTable))
      .delete(deleteOne(userType, userTable), returnResponse(userType, userTable))

  function val (schema) {
    return validate.body(schema)
  }

  function getAll (table) {
    return async function (req, res, next) {
      res.locals[table] = await db.all(table)
      .catch((err) => next(err))
      next()
    }
  }

  function clearTable (table) {
    return async function (req, res, next) {
      const rows = await db.clear(table)
      .catch((err) => next(err))
      res.locals[table] = rows
      res.status(204)
      next()
    }
  }

  // insert nto table
  function insertItem (type, table) {
    return async function (req, res, next) {
      let item
      if (type === 'post') {
        // if its an answer, send to ibm watson for analysis first
        analyze(req.body.atext)
          .then(blob => blob.json())
          .then(data => {
            const nlu = JSON.stringify(data)
            const body = {...req.body, nlu}
            db.create(table, body)
            .then(data => {
              item = data
              res.locals[type] = item[0]
              res.status(201)
              next()
            })
          })
          .catch((err) => next(err))
      } else {
        item = await db.create(table, req.body)
        .catch((err) => next(err))
        res.locals[type] = item[0]
        res.status(201)
        next()
      }
      // item = await db.create(table, req.body)
      //   .catch((err) => next(err))
    }
  }

  function getUsersItems (table) {
    return async function (req, res, next) {
      switch (table) {
        case 'posts':
          res.locals['posts'] = await db.getPostsByUserId(req.params.id)
          .catch((err) => next(err))
          next()
          break;
        default:
          break;
      }
    }
  }

  async function getUsersPosts (req, res, next) {
    res.locals['posts'] = await db.getPostsByUserId(req.params.id)
    .catch((err) => next(err))
    next()
  }

  function getOne (type, table) {
    return async function (req, res, next) {
      const item = await db.getById(table, req.params.id)
      .catch((err) => next(err))
      res.locals[type] = item && item[0]
      if (!res.locals[type]) {
        return next(new errors.NotFound('This item does not exist'))
      }
      next()
    }
  }

  async function getByUsername (req, res, next) {
    const user = await db.getUserByUsername(req.params.id)
    .catch((err) => next(err))
    res.locals.user = user && user[0]
    if (!res.locals.user) {
      return res.sendStatus(404)
      //next(new errors.NotFound('This user does not exist'))
    }
    next()
  }

  function updateOne (type, table) {
    return async function (req, res, next) {
      const item = Object.assign({}, res.locals[type][0], req.body)
      if (item.order) {
        item.position = item.order
        delete item.order
      }

      const updatedItem = await db.update(table, req.params.id, item)
      .catch((err) => next(err))
      res.locals[type] = updatedItem[0]
      next()
    }
  }

  function deleteOne (type, table) {
    return async function (req, res, next) {
      res.locals[type] = await db.deleteById(table, req.params.id)
      .catch((err) => next(err))
      res.status(204)
      next()
    }
  }

  function returnResponse (type, table) {
    return function (req, res) {
        // handle no responses here
      res.locals.baseUrl = `${req.protocol}://${req.get('host')}/db`
      res.json(transformResponse(res.locals, type, table))
    }
  }

  return router
}
